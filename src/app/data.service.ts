import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ICity } from './city';
import { IForecast } from './forecast';


@Injectable()
export class DataService {
  constructor(private http:HttpClient) {}

   getCities():Observable<ICity[]>{ 
   		return this.http.get<ICity[]>("/api/cities");
   }

   getForecast():Observable<IForecast[]>{
   		return this.http.get<IForecast[]>("/api/forecast");
   }

}
 