import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { CityTodayComponent} from './city-today/city-today.component';
import { ForecastComponent} from './forecast/forecast.component';

const routes: Routes = [
	{path: "cities", component: CityTodayComponent},
	{path: "forecast", component: ForecastComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [CityTodayComponent, ForecastComponent];
