import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule} from './app-routing.module';

import { DataService } from './data.service';
import { AppComponent } from './app.component';
import { ForecastComponent } from './forecast/forecast.component';
import { CityTodayComponent } from './city-today/city-today.component';


@NgModule({
  declarations: [
    AppComponent,
    ForecastComponent,
    CityTodayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
