import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-forecast',
  templateUrl: "./forecast.component.html",
  styleUrls: ['./forecast.component.css']
})
export class ForecastComponent implements OnInit {

  public cities = [];

  constructor(private _dataService: DataService) {

  }

  ngOnInit() {
  	this._dataService.getForecast()
      .subscribe(data => this.cities = data);
  }


}
