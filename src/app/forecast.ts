interface Forecastday {
    period: number;
    icon: string;
    icon_url: string;
    title: string;
    fcttext: string;
    fcttext_metric: string;
    pop: string;
}
interface TxtForecast {
    date: string;
    forecastday: Forecastday[];
}
interface Date {
    epoch: string;
    pretty: string;
    day: number;
    month: number;
    year: number;
    yday: number;
    hour: number;
    min: string;
    sec: number;
    isdst: string;
    monthname: string;
    monthname_short: string;
    weekday_short: string;
    weekday: string;
    ampm: string;
    tz_short: string;
    tz_long: string;
}
interface High {
    fahrenheit: string;
    celsius: string;
}
interface Low {
    fahrenheit: string;
    celsius: string;
}
interface QpfAllday {
    in: number;
    mm: number;
}
interface QpfDay {
    in: number;
    mm: number;
}
interface QpfNight {
    in: number;
    mm: number;
}
interface SnowAllday {
    in: number;
    cm: number;
}
interface SnowDay {
    in: number;
    cm: number;
}
interface SnowNight {
    in: number;
    cm: number;
}
interface Maxwind {
    mph: number;
    kph: number;
    dir: string;
    degrees: number;
}
interface Avewind {
    mph: number;
    kph: number;
    dir: string;
    degrees: number;
}
interface Forecastday2 {
    date: Date;
    period: number;
    high: High;
    low: Low;
    conditions: string;
    icon: string;
    icon_url: string;
    skyicon: string;
    pop: number;
    qpf_allday: QpfAllday;
    qpf_day: QpfDay;
    qpf_night: QpfNight;
    snow_allday: SnowAllday;
    snow_day: SnowDay;
    snow_night: SnowNight;
    maxwind: Maxwind;
    avewind: Avewind;
    avehumidity: number;
    maxhumidity: number;
    minhumidity: number;
}
interface Simpleforecast {
    forecastday: Forecastday2[];
}

export interface IForecast {
    txt_forecast: TxtForecast;
    simpleforecast: Simpleforecast;
}
