import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-city-today',
  templateUrl:'./city-today.component.html',
  styleUrls: ['./city-today.component.css']
})
export class CityTodayComponent implements OnInit {

  public cities = [];

  constructor(private _dataService: DataService) {

  }

  ngOnInit() {
  	this._dataService.getCities()
      .subscribe(data => this.cities = data);
  }

}
