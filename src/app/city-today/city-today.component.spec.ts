import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityTodayComponent } from './city-today.component';

describe('CityTodayComponent', () => {
  let component: CityTodayComponent;
  let fixture: ComponentFixture<CityTodayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityTodayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityTodayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
