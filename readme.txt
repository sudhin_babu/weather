1. install node, npm, packages
	move to same folder of this readme 
	and
	`` execute npm install

2. build frontend with ng-cli

	`` ng build

	* after making some changes we need to rebuild to reflect changes

3. start express server
	
	`` node server

	* notes changes will not be reflected immediately we have to stop and start node server

to change cities, api_key
===============
 got to --> server/routes/api.js
 and change CITIES

 to see raw express API response
 ============

 http://localhost:3000/api/cities
 http://localhost:3000/api/forecast
