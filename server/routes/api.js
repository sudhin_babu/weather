const express = require('express');
const router = express.Router();
var requestify = require('requestify'); 
var async = require('async'); 

// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    data: [],
    message: null
};


var apikey = "59a283f36749c192";
var weather_api = "http://api.wunderground.com/api/";

var CITIES = [
    "NY/New_york",
    "CA/Los_angeles",
    "NV/Las_vegas",
    "TX/Dallas"];




// Get cities
router.get('/cities', (req, res) => {
    var cities = [];

    async.each(CITIES, function(city, callback) {
        var url = weather_api+apikey+"/conditions/q/"+city+".json";
        requestify.get(url).then(function(res) {
            // Get the response body
            var res = res.getBody();
            cities.push(res.current_observation);
            console.log(" fetched"+city);
            callback();
        });
        console.log("not waiting -->"+city);

    }, function(err) {
        if( err ) {
          console.log('failed to process');
        } else {
          console.log('All cities have been processed successfully');
          response.data = cities;
          res.json(cities);
        }
    });


});

// get forecast

router.get('/forecast', (req, res) => {
    var cities = [];

    async.each(CITIES, function(city, callback) {
        var url = weather_api+apikey+"/forecast10day/q/"+city+".json";
        requestify.get(url).then(function(res) {
            // Get the response body
            var res = res.getBody();
            var obj = {
                 city: city,
                txt_forecast: res.forecast.txt_forecast,
                simpleforecast: res.forecast.simpleforecast
            }
            cities.push(obj);
            console.log(" fetched"+city);
            callback();
        });
        console.log("not waiting -->"+city);

    }, function(err) {
        if( err ) {
          console.log('failed to process');
        } else {
          console.log('All cities forecast have been processed successfully');
          response.data = cities;
          res.json(cities);
        }
    });


});

module.exports = router;